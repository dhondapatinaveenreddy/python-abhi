# Python3 is required to execute this script
from main import *

# S3 bucket has files in it
s3_bucket_name = "erieins-actuarial-dev-actuarial-sagemaker-us-east-2"

# Directory to copy from awspps server
# Need input from Sapiens Team
directory_to_copy = "/opt/data/calabrio/inputdata/fnol_historical_data"


# Use keys auth named profile
# Assumes named profile is already setup on the machine for erieins-commercial-env
aws_profile = use_keys_auth("dev")

##Upload files to s3
upload_file_nas_to_s3(directory_to_copy, s3_bucket_name)

# Successful run of the script
sys.exit(0)
