# Python3 is required to execute this script
import os
import boto3
from botocore.exceptions import ClientError
import logging
import sys
import datetime


#Uploading single file to S3
def upload_file_nas_to_s3(directory_to_copy,s3_bucket_name):
  try:
    s3_resource = boto3.client("s3")
    bucket_name = s3_bucket_name
    root_path = directory_to_copy
    for root,dirs,files in os.walk(root_path):
      print(files)
      for file in files:
        os.rename(file, file+'_'+str(datetime.datetime.now()))
        s3_resource.upload_file(os.path.join(root,file),bucket_name,'tempdir/'+file)
        print(file +'Uploaded')
        status_string="[-] Upload Objects Successful"
        return status_string
        os.remove(file)
  except ClientError as e:
    logging.error(e)
    status_string = e
    print(status_string)
    sys.exit(101)


# sys.exit code 100
def use_keys_auth(env):
  # Setup Authentication to AWS
  try:
    boto3.setup_default_session(region_name="us-east-2", profile_name="saml-erieins-actuarial-dev")
  except:
    e = sys.exc_info()[0]
    e1 = sys.exc_info()[1]
    e2 = sys.exc_info()[2]
    print(" [!] ")
    print(e)
    print(e1)
    print(e2)
    sys.exit(100)
  return "saml-erieins-actuarial-dev"
